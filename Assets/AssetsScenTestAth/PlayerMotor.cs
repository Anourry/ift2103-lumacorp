﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour {

    [SerializeField]
    private Camera cam;
    
    private bool falling = false;
    
    private Vector3 jumpForce = Vector3.zero;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;

    [SerializeField]
    private float cameraRotationLimit = 55f;
    

    
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    //Obtention du vecteur de mvt
    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    //Obtention des rotation
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }

    //Obtention des rotation de camera
    public void RotateCamera(float _cameraRotationX)
    {
        cameraRotationX = _cameraRotationX;
    }

    void OnCollisionStay (Collision col)
    {
        falling = false;
    }

    //Obtention du saut
    public void ApplyJump(Vector3 _jumpForce)
    {
        jumpForce = Vector3.zero;
        if (falling == false)
        {
            jumpForce = _jumpForce;
            falling = true;
        }
        
    }

    void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }

    

    //Realisation du mvt
    void PerformMovement()
    {
        if (velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.deltaTime);
        }

        //Saut
        if(jumpForce != Vector3.zero)
        {
            rb.AddForce(jumpForce * Time.fixedDeltaTime, ForceMode.Acceleration); //On ignore la masse du perso
        }
    }

    //Realisation des rotations
    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));

        if(cam != null)
        {
            //Rotation de la camera avec une limitation angulaire
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f); 
        }

    }





}
