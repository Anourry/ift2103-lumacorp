﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour {

    public GameObject player;
    public GameObject targetPoint;
    public float speed;

    Transform unit;
    Vector3 startPoint;
    int move;
    bool istargetreach = false;
    bool chasingState = false;
	// Use this for initialization
	void Start () {
        unit = this.GetComponent<Transform>();
        startPoint = new Vector3(unit.transform.localPosition.x, unit.transform.localPosition.y, unit.transform.localPosition.z);
	}
	
	// Update is called once per frame
	void Update () {
        if (isEnemyInSight())
            Chasing();
        else if (chasingState == true)
            Retreat();
        else
            patrol();
	}

    void patrol()
    {
        if (player.transform.localPosition.z < unit.transform.localPosition.z && istargetreach == false)
        {
            unit.transform.Translate(Vector3.back * speed * Time.deltaTime);
            move++;
        }
        else
        {
            istargetreach = true;
        }
        if (istargetreach == true)
        {
            unit.transform.Translate(Vector3.forward * speed * Time.deltaTime);
            move--;
        }
        if (move == 0)
        {
            istargetreach = false;
        }
    }

    bool isEnemyInSight()
    {
        Collider[] hitColliders = Physics.OverlapSphere(unit.transform.position, 5f);

        int i = 0;
        while (i < hitColliders.Length)
        {
            
            if (hitColliders[i].transform.gameObject.name == player.name)
            {
                return (true);
            }
            i++;
        }
        return (false);
    }

    void Chasing()
    {
        float step = speed * Time.deltaTime;
        unit.transform.position = Vector3.MoveTowards(unit.transform.position, player.transform.position, step);
        chasingState = true;
    }

    void Retreat()
    {
        float step = speed * Time.deltaTime;
        unit.transform.position = Vector3.MoveTowards(unit.transform.position, startPoint, step);
        if (unit.transform.position == startPoint)
            chasingState = false;
    }


}