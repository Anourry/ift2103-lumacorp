﻿using UnityEngine;
using System.Collections;

public class TimerScript : MonoBehaviour
{

    #region Properties
    // Use this for initialization
    private float time = 0;
    private TextMesh t;
    public bool IsTime = false;
    #endregion


    #region Methods
    void Start () {
        t = (TextMesh)gameObject.GetComponent(typeof(TextMesh));
        
	}
	
	// Update is called once per frame
	void Update () {
        if (IsTime == true)
        {
            time += Time.deltaTime;
            t.text = time.ToString();
            if (time >= 15)
            {
                Debug.Log("Exit game");
                Application.LoadLevel(Application.loadedLevel);
            }
        }
	}
    #endregion

}
