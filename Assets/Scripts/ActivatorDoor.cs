﻿using UnityEngine;
using System.Collections;

public class ActivatorDoor : MonoBehaviour {

    // Use this for initialization
    public GameObject door1;
    public GameObject door2;
    private bool porteOuverte;

    private Transform door1Transform;
    private Transform door2Transform;
    
    void Start()
    {
        door1Transform = door1.GetComponent(typeof(Transform)) as Transform;
        door2Transform = door2.GetComponent(typeof(Transform)) as Transform;
        porteOuverte = false;
    }
    
    void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !porteOuverte)
        {
            door1Transform.Translate(0, 0, 1);
            door2Transform.Translate(0, 0, -1);
            porteOuverte = true;
        }
    }
}
