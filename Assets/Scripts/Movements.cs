﻿using UnityEngine;
using System.Collections;

public class Movements : MonoBehaviour {

    public GameObject cam;
    public GameObject character;
    public int characSpeed;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    Vector3 translation;
    Transform camTransform;
    Transform characTransform;
    CharacterController characterController;

	// Use this for initialization
	void Start () {
        camTransform = cam.GetComponent<Transform>();
        characterController = this.GetComponent<CharacterController>();
        characTransform = character.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        handleCharacMovement();
	}

    void handleCharacMovement()
    {
        float frontMovement = 0f;
        float sideMovement = 0f;
        
        
        if (Input.GetButton("CharacForwardMovement"))
        {
            frontMovement = characSpeed * Time.deltaTime;
        }
        if (Input.GetButton("CharacBackwardMovement"))
        {
            frontMovement = -characSpeed * Time.deltaTime;
        }
        if (Input.GetButton("CharacLeftMovement"))
        {
            sideMovement = -characSpeed * Time.deltaTime;
        }
        if (Input.GetButton("CharacRightMovement"))
        {
            sideMovement = characSpeed * Time.deltaTime;
        }
        if (Input.GetButton("Jump"))
        {
            Debug.Log("JUMP");
            translation.y = jumpSpeed * Time.deltaTime;
        }
        translation = (sideMovement * camTransform.right + frontMovement * camTransform.forward) * characSpeed;

        characterController.SimpleMove(translation);
    }

    
}
