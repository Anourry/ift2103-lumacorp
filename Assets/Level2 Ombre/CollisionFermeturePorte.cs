﻿using UnityEngine;
using System.Collections;

public class CollisionFermeturePorte : MonoBehaviour
{

    public GameObject porte;
    private bool porteClose = false;

    //Si le joueur est dans la zone de detection du bouton
    void OnTriggerStay(Collider other)
    {
        if (!porteClose)
        {

            GameObject Player = other.gameObject;
     
            //On recupere le script
            Animator anim = porte.GetComponent<Animator>();
            //On enclenche l'ouverture
            anim.Play("FermeturePortes");
            porteClose = true;

        }

    }

}
