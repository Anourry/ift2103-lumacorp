﻿using UnityEngine;
using System.Collections;

public class FlashLight : MonoBehaviour {
    [SerializeField]
    public Light lightVar;

    void Awake()
    {
        lightVar.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            lightVar.enabled = !lightVar.enabled;
        }
    }
}
