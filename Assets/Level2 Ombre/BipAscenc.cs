﻿using UnityEngine;
using System.Collections;

public class BipAscenc : MonoBehaviour {

    public AudioClip bip;

	public void bipSound()
    {
        AudioSource.PlayClipAtPoint(bip, transform.position);
    }
}
