﻿using UnityEngine;
using System.Collections;

public class Mouvement : MonoBehaviour {

    public float playerAcceleration = 500f;
    public GameObject cameraObject;
    private Rigidbody rigidBody;
    private MouseLook camera;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        camera = cameraObject.GetComponent(typeof(MouseLook)) as MouseLook;
    }
    // Update is called once per frame
    void Update ()
    {
        transform.rotation = Quaternion.Euler(0, camera.yRotation, 0);
        rigidBody.AddRelativeForce(Input.GetAxis("Horizontal") * playerAcceleration * Time.deltaTime, 0, Input.GetAxis("Vertical") * playerAcceleration * Time.deltaTime);

    }
}
