﻿using UnityEngine;
using System.Collections;

public class InvisibleScript : MonoBehaviour {

	// Use this for initialization
    public bool invisible;
	void Start () {
        GetComponent<MeshRenderer>().enabled = false;
        this.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (invisible == true)
        {
            GetComponent<MeshRenderer>().enabled = true;
            //gameObject.SetActive(true);
            //Debug.Log("la valeur de invisible est = " + invisible);
        }
        else
        {
            GetComponent<MeshRenderer>().enabled = false;
            //this.gameObject.SetActive(false);
            //Debug.Log("la valeur de invisible est = " + invisible);
        }
	}
}
