﻿using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour {

    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float lookSensitivity = 3f;
    [SerializeField]
    private float jumpForce = 10000f;

    
    private PlayerMotor motor;

    void Start()
    {
        motor = GetComponent<PlayerMotor>();
    }

    void Update()
    {
        //Calcul des mouvements
        float _xMov = Input.GetAxisRaw("Horizontal");
        float _zMov = Input.GetAxisRaw("Vertical");

        Vector3 movHorizontal = transform.right * _xMov; //(1,0,0) ou (0,0,0)
        Vector3 movVertical = transform.forward * _zMov;

        //Vector de mouvement
        Vector3 _velocity = (movHorizontal + movVertical).normalized * speed;

        //Application du mvt
        motor.Move(_velocity);

        //Calcul des rotation (en vector3d) Que de maniere horizontale
        float _yRot = Input.GetAxisRaw("Mouse X");

        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * lookSensitivity;

        //Aplication de la rotation
        motor.Rotate(_rotation);

        //Calcul des rotation camera
        float _xRot = Input.GetAxisRaw("Mouse Y");

        float _cameraRotationX = _xRot * lookSensitivity;

        //Aplication de la rotation camera
        motor.RotateCamera(_cameraRotationX);

        //Saut
        Vector3 _jumpForce = Vector3.zero;
        
        if (Input.GetButton("Jump"))
        {
            _jumpForce = Vector3.up * jumpForce;
        }

        //Application du saut
        motor.ApplyJump(_jumpForce);


    }
}
