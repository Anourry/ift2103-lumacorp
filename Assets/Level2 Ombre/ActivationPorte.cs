﻿using UnityEngine;
using System.Collections;

public class ActivationPorte : MonoBehaviour
{

    public GameObject porte;
    public bool bouttonActif = false;
    private bool collBouton =false;

    //Si le joueur est dans la zone de detection du bouton
    void OnTriggerStay(Collider other)
    {
        GameObject Player = other.gameObject;

        //Si le joueur appuye sur le bouton
            if (Player.name == "Player" && Input.GetKeyDown("mouse 1"))
        {
            //On recupere le script
            Animator anim = porte.GetComponent<Animator>();
            //On enclenche l'ouverture
            anim.Play("OuverturePortes");
            bouttonActif = true;
        }
    }


}
