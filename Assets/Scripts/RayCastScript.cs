﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RayCastScript : MonoBehaviour {

	// Use this for initialization
    public Transform cube;
    public float raycastDistStairs = 15f;
    public string tagCheck = "InvTag";
    public bool checkAllTags = false;
    public Text SwitchText;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit[] foundHit;
        foundHit = Physics.RaycastAll(transform.position, transform.forward, raycastDistStairs);

        for (int i = 0; i < foundHit.Length; i++)
        {
            RaycastHit hit = foundHit[i];
            if (hit.transform.tag == tagCheck) {
                hit.transform.GetComponent<InvisibleScript>().invisible = true;
            }
            else if (hit.transform.tag == tagCheck && hit.transform.GetComponent<InvisibleScript>())
            {
                    Debug.Log("hit gonna false");
                    hit.transform.GetComponent<InvisibleScript>().invisible = false;
            }
            if (hit.transform.tag == "SwitchWorld")
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (hit.transform.GetComponent<ChangeWorldScript>().on_off == false)
                    {
                        //Debug.Log("Trigger key down Off");
                        //SwitchText.
                        RenderSettings.reflectionIntensity = 0.50f;
                        hit.transform.GetComponent<ChangeWorldScript>().light.GetComponent<Light>().enabled = false;
                        hit.transform.GetComponent<ChangeWorldScript>().on_off = true;
                        Debug.Log("St 1" + hit.transform.GetComponent<ChangeWorldScript>().st.Length);
                        for (int a = 0; a < hit.transform.GetComponent<ChangeWorldScript>().st.Length; ++a)
                        {
                            hit.transform.GetComponent<ChangeWorldScript>().st[a].gameObject.SetActive(true);
                        }

                    }
                    else if (hit.transform.GetComponent<ChangeWorldScript>().on_off == true)
                    {
                        //Debug.Log("Trigger key down On");
                        RenderSettings.reflectionIntensity = 0.75f;
                        hit.transform.GetComponent<ChangeWorldScript>().light.GetComponent<Light>().enabled = true;
                        hit.transform.GetComponent<ChangeWorldScript>().on_off = false;
                        Debug.Log("St 2"+hit.transform.GetComponent<ChangeWorldScript>().st.Length);
                        for (int e = 0; e < hit.transform.GetComponent<ChangeWorldScript>().st.Length; ++e)
                        {
                            hit.transform.GetComponent<ChangeWorldScript>().st[e].SetActive(false);
                        }
                    }
                }
            }
        }
	}
}
