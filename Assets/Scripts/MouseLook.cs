﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {
    public float lookSensitivity = 5;
    public float yRotation;
    public float xRotation;
    public float CurrentXRotation;
    public float CurrentYRotation;
    public float yRotationV;
    public float xRotationV;
    public float lookSmoothness = 0.1f;
        
    // Update is called once per frame
    void Update () {

        yRotation += Input.GetAxis("Mouse X") * lookSensitivity;
        xRotation -= Input.GetAxis("Mouse Y") * lookSensitivity;

        xRotation = Mathf.Clamp(xRotation, -80, 100);

        CurrentXRotation = Mathf.SmoothDamp(CurrentXRotation, xRotation, ref xRotationV, lookSmoothness);
        CurrentYRotation = Mathf.SmoothDamp(CurrentXRotation, yRotation, ref yRotationV, lookSmoothness);

        transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
    }
}
