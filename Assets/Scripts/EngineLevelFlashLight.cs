using UnityEngine;
using System;

public class EngineLevelFlashLight : MonoBehaviour {
	
	public GameObject player;
	public GameObject camera;
    public GameObject reference;
    public GameObject flashLight;
    public GameObject button;
    public GameObject vase;
    private Light lightVar;
    private Transform playerTransform;
	private Transform cameraTransform;
    private Transform referenceTransform;
 
	

	//Position du point 
	private Vector3 pointPosition ;
	//Marge entre la position du joueur et le point
	public float margePosition = 2.0f;

    //Distance maximal en Y
    public float margeRotationY = 8f;
    //Distance maximal en X
    public float margeRotationX = 5f;



    private Rect windowRect = new Rect(20, 20, 300, 50);
    private bool EnigmeResolved = false;

	private Vector3 validRotation;

	// Use this for initialization
	void Start () {
	
		playerTransform = player.GetComponent(typeof(Transform)) as Transform;
		cameraTransform = camera.GetComponent(typeof(Transform)) as Transform;
        referenceTransform = reference.GetComponent(typeof(Transform)) as Transform;
        lightVar = flashLight.GetComponent<Light>();

        button.SetActive(false);

        pointPosition = referenceTransform.position;
        validRotation = referenceTransform.rotation.eulerAngles;
    }
	
	// Update is called once per frame
	void Update () {

		Vector3 position = playerTransform.position;
		float yRotation = playerTransform.rotation.eulerAngles.y;
		float xRotation = cameraTransform.rotation.eulerAngles.x;

		float distance = Vector3.Distance(position, pointPosition);
		float distanceY = Math.Abs(validRotation.y- yRotation);
		float distanceX = Math.Abs(validRotation.x- xRotation);

		if (distance < margePosition && distanceY < margeRotationY && distanceX < margeRotationX && lightVar.enabled)
		{
			EnigmeResolved = true;
            button.SetActive(true);
            vase.SetActive(false);
        }
	}

	void OnGUI() {
		windowRect = GUI.Window(0, windowRect, DoMyWindow, "Message");
	}

	void DoMyWindow(int windowID) {
		if(EnigmeResolved)
			GUI.TextArea(new Rect (10, 20, 100, 20), "Enigme réussie");
	}
}

